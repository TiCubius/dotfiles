#!/bin/bash

pkill polybar; sleep 1

# @see https://github.com/polybar/polybar/issues/763#issuecomment-331604987
for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    (MONITOR=$m polybar -r primary-right) &
    (MONITOR=$m polybar -r primary-center) &
    (MONITOR=$m polybar -r primary-left) &
done
