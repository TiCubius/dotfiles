#!/bin/bash
clear

read -p "What is the environment ? " environment
echo $environment
if [[ "$environment" != "home" && "$environment" != "work" ]]; then
    echo "The environment must be home or work."
    exit 1
fi

source mapping.sh

NETWORK_INTERFACE_NAME=`ip link | grep -v 'DOWN' | awk -F: '$0 !~ "lo|vir|wl|docker|^[^0-9]"{print $2;getline}' | tr -d '[:space:]'`
for realPath in ${!filesMapping[@]}; do
    mkdir -p $(dirname "${filesMapping[${realPath}]}")
    cp "${realPath}" "${filesMapping[${realPath}]}"

    sed -i 's?'`pwd`'?!!DOTFILES_DIRECTORY_PATH!!?' "${filesMapping[${realPath}]}"
    sed -i 's?'${NETWORK_INTERFACE_NAME}'?!!NETWORK_INTERFACE_NAME!!?' "${filesMapping[${realPath}]}"
done
