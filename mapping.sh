#!/bin/bash

declare -A filesMapping

filesMapping["$(echo ~/.Xresources)"]="$(pwd)/xrdb/.Xresources"
filesMapping["$(echo ~/.config/rofi/config.rasi)"]="$(pwd)/rofi/config.rasi"
filesMapping["$(echo ~/.config/rofi/colors.rasi)"]="$(pwd)/rofi/colors.rasi"

if [[ "$environment" == "home" ]]; then
    filesMapping["$(echo ~/.config/picom.conf)"]="$(pwd)/picom/home.conf"
    filesMapping["$(echo ~/.config/polybar/config.ini)"]="$(pwd)/polybar/home.ini"
    filesMapping["$(echo ~/.i3/config)"]="$(pwd)/i3/home"
elif [[ $environment == "work" ]]; then
    filesMapping["$(echo ~/.config/picom.conf)"]="$(pwd)/picom/work.conf"
    filesMapping["$(echo ~/.config/polybar/config.ini)"]="$(pwd)/polybar/work.ini"
    filesMapping["$(echo ~/.i3/config)"]="$(pwd)/i3/work"
fi