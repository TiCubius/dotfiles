#!/bin/bash
clear

read -p "What is the environment ? " environment
echo $environment
if [[ "$environment" != "home" && "$environment" != "work" ]]; then
    echo "The environment must be home or work."
    exit 1
fi

sudo sed -in 's/#EnableAUR.*/EnableAUR/g' /etc/pamac.conf
sudo sed -in 's/MaxParallelDownloads.*/MaxParallelDownloads = 10/g' /etc/pamac.conf

echo " !===| UPGRADING SYSTEM |===!"
sleep 2
sudo pamac upgrade --no-confirm


echo " !===| INSTALLING PACKAGES |===!"
sleep 2

APPS=(firefox chromium vscode phpstorm phpstorm-jre nextcloud-client synergy keeweb-desktop-bin obs-studio thunderbird teams slack-desktop joplin-desktop)
TOOLS=(python2-bin feh neovim flameshot docker docker-compose antigen-git pulseaudio pulseaudio-alsa pulseaudio-bluetooth keeweb-desktop-bin arandr)
UI=(cairo polybar rofi ttf-jetbrains-mono ttf-font-awesome nerd-fonts-complete)
sudo pamac install "${APPS[@]}" "${TOOLS[@]}" "${UI[@]}" --no-confirm


echo " !===| SETTING-UP SHELL |===!"
sleep 2
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

grep -qxF 'source /usr/share/zsh/share/antigen.zsh' ~/.zshrc || echo 'source /usr/share/zsh/share/antigen.zsh' >> ~/.zshrc
grep -qxF 'antigen bundle zsh-users/zsh-autosuggestions' ~/.zshrc || echo 'antigen bundle zsh-users/zsh-autosuggestions' >> ~/.zshrc
grep -qxF 'antigen theme bureau' ~/.zshrc || echo 'antigen theme bureau' >> ~/.zshrc
grep -qxF 'antigen apply' ~/.zshrc || echo 'antigen apply' >> ~/.zshrc


echo " !===| SETTING-UP CONFIGURATIONS |===!"
source mapping.sh

NETWORK_INTERFACE_NAME=`ip link | grep -v 'DOWN' | awk -F: '$0 !~ "lo|vir|wl|docker|^[^0-9]"{print $2;getline}' | tr -d '[:space:]'`
for realPath in ${!filesMapping[@]}; do
    mkdir -p $(dirname "${realPath}")
    cp "${filesMapping[${realPath}]}" "${realPath}" 

    sed -i 's?!!DOTFILES_DIRECTORY_PATH!!?'`pwd`'?' "${realPath}"
    sed -i 's?!!NETWORK_INTERFACE_NAME!!?'${NETWORK_INTERFACE_NAME}'?' "${realPath}"
done

systemctl --user enable pulseaudio
systemctl --user start pulseaudio

nitrogen `pwd`/i3
i3-msg restart
